document.querySelector("#add-row-action").addEventListener(
    "click",
    () => {
        let tbody = document.querySelector("#my-table > tbody");

        let rowTemplate = document.querySelector("#row-template").content.firstElementChild;
        let rowCopy = document.importNode(rowTemplate, true);

        for (let i = 0; i < rowCopy.childElementCount; i++) {
            rowCopy.children[i].innerHTML = i + ": " + Math.floor(Math.random() * 10);
        }

        tbody.appendChild(rowCopy);
    }
);