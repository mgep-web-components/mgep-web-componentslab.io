let template = `
<style>
  .container {
    container-type: inline-size;
  }
  
  .item-list {
    display: flex;
    flex-direction: column;
    gap: 1em;
    border-left: 1px solid black;
  }
  
  .item-list > * {
    position: relative;
    padding-left: 1em;
  }
  
  .item-list > *::before {
    content: "▸";
    position:absolute;
    left: 0;
    top: calc(50% - 0.7em);
  }
  
  
  @container (min-width: 700px) {
    
    .item-list {
      flex-direction: row;
      border-left: 0;
    }
    .item-list > *::before {
      content: "";
    }
    .item-list > *::after{
      display: block;
      content: "△";
      border-bottom: 1px solid black;
      margin: 0 -1em;
      padding-left: 50%;
      
    }
  }

</style>

<div class="container">
  <slot class="item-list"></slot>
</div>
`;

class ContainerQueryComponent extends HTMLElement {
  constructor() {
    super();
    this._shadowRoot = this.attachShadow({ mode: 'open' });
    /*if(this.slot.default)*/
    this._shadowRoot.innerHTML = template;
  }
}
customElements.define('container-query-component', ContainerQueryComponent);