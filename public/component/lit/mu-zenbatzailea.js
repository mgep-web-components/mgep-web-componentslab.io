import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import 'https://mgep-web-components.gitlab.io/component/mu-button/mu-button.js';
class MUZenbatzailea extends LitElement {
    interval = null;

    static styles = css`
        span {
            display:inline-block; 
            min-width: 2em; 
            text-align: center; 
        }
    `;
    static properties = {
        counter: { type: Number },
    };

    constructor() {
        super();
        this.counter = 0;
    }

    add() {
        this.counter++;
    }

    remove() {
        this.counter--;
    }

    adding() {
        this.stop();
        this.remove();
        this.interval = setInterval(()=> {this.remove()}, 200);
    }

    removing() {
        this.stop();
        this.add();
        this.interval = setInterval(()=> {this.add()}, 200);
    }

    stop() {
        clearInterval(this.interval);
    }


    render() {
        return html`
            <mu-button @mousedown="${this.adding}"
                    @mouseup="${this.stop}"
                    @mouseout="${this.stop}"
                    part="button left-button">-</mu-button>
            <span part="value">${this.counter}</span>
            <mu-button @mousedown="${this.removing}"
                    @mouseup="${this.stop}"
                    @mouseout="${this.stop}"
                    part="button right-button">+</mu-button>
        `;
    }

}

customElements.define('mu-zenbatzailea', MUZenbatzailea);