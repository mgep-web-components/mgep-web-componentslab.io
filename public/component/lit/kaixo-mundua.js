import { LitElement, html, css } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';

export class KaixoMundua extends LitElement {
    static styles = css`p { color: blue }`;
    static properties = {
        name: { type: String },
    };

    constructor() {
        super();
        this.name = 'Ezezagun';
    }


    render() {
        return html`<p>Kaixo, ${this.name}!</p>`;
    }

}

customElements.define('kaixo-mundua', KaixoMundua);