import { LitElement, html, css,  unsafeCSS } from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';
import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/highlight.min.js';
// Hau ez dabil, ez dakit zergatik

export class HighlightSyntax extends LitElement {
    static styles = css`*{font-weight: bold;}`;
    static properties = {
        language: { type: String },
    };

    constructor() {
        super();
        this.language = 'nohighlight';
        /* this is not working */
        fetch('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.7.0/styles/default.min.css')
            .then(
                (response) => this.styles = unsafeCSS(response.text)
            );
    }

    highlightAll() {
        hljs.highlightAll();
    }


    render() {
        return html`
        <p>with slot</p>
        <pre>
            <code class="${this.language}">
                <slot></slot>
            </code>
        </pre>
        <p>direct print</p>
        <pre>
            <code class="language-json">
            [
                {
                "title": "apples",
                "count": [12000, 20000],
                "description": {"text": "...", "sensitive": false}
                },
                {
                "title": "oranges",
                "count": [17500, null],
                "description": {"text": "...", "sensitive": false}
                }
            ]
            </code>
        </pre>
        <button @click="${this.highlightAll}">Highlight all</button>
        `;
    }

}

customElements.define('highlight-syntax', HighlightSyntax);