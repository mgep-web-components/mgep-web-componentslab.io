let template = `
<style>

button {
    --mgep-hue:  183;
    --mgep-saturation: 100%;
    --mgep-light: 34%;
    color: white;
    border: 0;
    padding: 0.5em 1em;
    border-radius: 0.25em;
    background-color: hsl(var(--mgep-hue),var(--mgep-saturation),var(--mgep-light));
}

button:hover {
    background-color: hsl(var(--mgep-hue), var(--mgep-saturation), 40%);
}

button:active {
    background-color: hsl(var(--mgep-hue), var(--mgep-saturation), 44%);
}

</style>

<button part="button">
    <slot></slot>
</button>
`;

class MUButton extends HTMLElement {
    constructor() {
        super();
        this._shadowRoot = this.attachShadow({ mode: 'open' });
        /*if(this.slot.default)*/
        this._shadowRoot.innerHTML = template;
    }
}
customElements.define('mu-button', MUButton);