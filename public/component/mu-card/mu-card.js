let base_css = `
<link rel="stylesheet" href="https://mgep-web-components.gitlab.io/component/mu-card/css/mu-card.css"/>
`;
let variant_css = {
    'basic': '',
    'flex': '<link rel="stylesheet" href="https://mgep-web-components.gitlab.io/component/mu-card/css/mu-flex-card.css"/>',
    'subgrid': '<link rel="stylesheet" href="https://mgep-web-components.gitlab.io/component/mu-card/css/mu-subgrid-card.css"/>'
}

let html_templates = {
    'header': `
        <header id="mu-card__header" part="header">
            <slot name="header"></slot>
        </header>
    `,
    'default': `
        <div id="mu-card__content" part="content">
            <slot></slot>
        </div>
    `,
    'footer': `
        <footer id="mu-card__footer" part="footer">
            <slot name="footer"></slot>
        </footer>
    `
}

class MUCard extends HTMLElement{
    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});

        let variant = this.getAttribute("variant") || 'basic';
        let html_template = base_css + variant_css[variant] +`
            <article id="mu-card" part="container">
        `;
        console.log(this);
        if(this.querySelector('[slot="header"]'))   html_template += html_templates['header'];
        /*if(this.slot.default)*/
        html_template += html_templates['default'];
        if(this.querySelector('[slot="footer"]'))   html_template += html_templates['footer'];


        this._shadowRoot.innerHTML = html_template + `
            </article>
        `;
    }
};
customElements.define('mu-card', MUCard);